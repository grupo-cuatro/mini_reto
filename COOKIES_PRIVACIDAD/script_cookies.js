document.addEventListener('DOMContentLoaded', function () {
  // Mostrar el pop-up después de cargar la página DE inicio
  setTimeout(function () {
    mostrarCookiePopup();
  }, 1000); // Mostrar después de 1 segundo 

  // Funciones para aceptar y rechazar cookies
  window.aceptarCookies = function () {
    guardarCookies();
    ocultarCookiePopup();
  };

  window.rechazarCookies = function () {
    eliminarCookies();
    ocultarCookiePopup();
  };

  // Funciones auxiliares
  function mostrarCookiePopup() {
    document.getElementById('cookie-popup').style.display = 'block';
  }

  function ocultarCookiePopup() {
    document.getElementById('cookie-popup').style.display = 'none';
  }

  function guardarCookies() {

  }

  function eliminarCookies() {

  }
});
