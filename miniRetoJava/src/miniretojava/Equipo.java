/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package miniretojava;

public class Equipo {

    private String nombreEquipo;
    private String entrenador;
    private String[] jugadores;
    private static int numEquipos = 0;

    // Constructor
    public Equipo(String nombreEquipo, String entrenador) {
        this.nombreEquipo = nombreEquipo;
        this.entrenador = entrenador;
        this.jugadores = new String[0]; 
        numEquipos++;
    }

    // Método para obtener el número de equipos creados.
    public static int getNumEquipos() {
        return numEquipos;
    }

    // Método para añadir un jugador al equipo.
    public void añadirJugador(String jugador) {
        String[] nuevoArray = new String[jugadores.length + 1];
        System.arraycopy(jugadores, 0, nuevoArray, 0, jugadores.length);
        nuevoArray[jugadores.length] = jugador;
        jugadores = nuevoArray;
    }

    // Método para eliminar un jugador del equipo.
    public void eliminarJugador(String jugador) {
        int indice = -1;
        for (int i = 0; i < jugadores.length; i++) {
            if (jugadores[i].equals(jugador)) {
                indice = i;
                break;
            }
        }
        if (indice != -1) {
            String[] nuevoArray = new String[jugadores.length - 1];
            System.arraycopy(jugadores, 0, nuevoArray, 0, indice);
            System.arraycopy(jugadores, indice + 1, nuevoArray, indice, jugadores.length - indice - 1);
            jugadores = nuevoArray;
        }
    }

    // Método para buscar un jugador en el equipo.
    public boolean buscarJugador(String jugador) {
        for (String j : jugadores) {
            if (j.equals(jugador)) {
                return true;
            }
        }
        return false;
    }

    
    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(String entrenador) {
        this.entrenador = entrenador;
    }

    public String[] getJugadores() {
        return jugadores;
    }

    public void setJugadores(String[] jugadores) {
        this.jugadores = jugadores;
    }
}
