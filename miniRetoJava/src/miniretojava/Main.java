/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package miniretojava;

public class Main {

    public static void main(String[] args) {
        // Crear un nuevo equipo
        System.out.println("clase crear un nuevo equipo");
        Equipo equipo1 = new Equipo("Equipo A", "Entrenador A");
        System.out.println("------------------------");

        // Añadir jugadores al equipo
        System.out.println("clase añadir jugadores");
        equipo1.añadirJugador("Jugador1");
        equipo1.añadirJugador("Jugador2");
        equipo1.añadirJugador("Jugador3");
        System.out.println("------------------------");

        // Mostrar jugadores del equipo
        System.out.println("clase mostrar jugadores");
        System.out.println("Jugadores del " + equipo1.getNombreEquipo() + ":");
        for (String jugador : equipo1.getJugadores()) {
            System.out.println("- " + jugador);
        }
        System.out.println("------------------------");

        // Buscar un jugador en el equipo
        System.out.println("clase buscar un jugador en el equipo");
        String jugadorBuscado = "Jugador2";
        if (equipo1.buscarJugador(jugadorBuscado)) {
            System.out.println(jugadorBuscado + " está en el equipo.");
        } else {
            System.out.println(jugadorBuscado + " no está en el equipo.");
        }
        System.out.println("------------------------");

        // Eliminar un jugador del equipo
        System.out.println("clase eliminar un jugador");
        equipo1.eliminarJugador("Jugador2");
        System.out.println("------------------------");
        
        // Comprobar jugadores eliminados
        System.out.println("clase buscar un jugador en el equipo");
        if (equipo1.buscarJugador(jugadorBuscado)) {
            System.out.println(jugadorBuscado + " está en el equipo.");
        } else {
            System.out.println(jugadorBuscado + " no está en el equipo.");
        }
        System.out.println("------------------------");
        
        
        // Mostrar jugadores actualizados del equipo
        System.out.println("clase mostrar jugadores (actiualizado)");
        System.out.println("Jugadores del " + equipo1.getNombreEquipo());
        for (String jugador : equipo1.getJugadores()) {
            System.out.println("- " + jugador);
        }
        System.out.println("------------------------");

        // Mostrar el número total de equipos creados
        System.out.println("clase mostrar el numero total de equipos creados");
        System.out.println("Número total de equipos creados: " + Equipo.getNumEquipos());
        
    }
}
